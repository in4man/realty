import { Directive, ElementRef, OnInit, Input, Injector } from '@angular/core';
import { OverlayRef, OverlayConfig, Overlay, GlobalPositionStrategy } from '@angular/cdk/overlay';
import { Observable } from 'rxjs';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';

// import { DynamicOverlay } from './dynamic-overlay';
import { LoaderData } from '../lib/loader/loader.config';
import { LoaderComponent } from '../lib/loader/loader.component';
import { DynamicOverlay } from '../lib/loader/dynamic-overlay.service';
import { DynamicOverlayContainer } from '../lib/loader/dynamic-overlay-container.service';
import { ResponsivePositionStrategy } from '../responsive-position-strategy';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[app-overlayLoading]'
})
export class OverlayLoadingDirective implements OnInit {
  @Input('app-overlayLoading') toggler: Observable<boolean>;
  @Input() loaderData: LoaderData;

  private overlayRef: OverlayRef;
  
  @Input()
  setOverlayRef(ref) {
    this.overlayRef = ref;
  }

  constructor(
    private host: ElementRef,
    private overlay: Overlay,
    private dynamicOverlay: DynamicOverlay,
    private parentInjector: Injector,
    private cont: DynamicOverlayContainer 
  ) {
    // this.setOverlayRef(overlay);
    this.setOverlayRef(dynamicOverlay);
  }

  ngOnInit() {

    this.toggler.subscribe(show => {
      if (show) {
        const injector = this.getInjector(this.loaderData, this.parentInjector);
        const loaderPortal = new ComponentPortal(
          LoaderComponent,
          null,
          injector
        );


        const config = new OverlayConfig({
          backdropClass: 'backdropOverlay',
          panelClass: 'panel-Container',
          positionStrategy: new ResponsivePositionStrategy((config, style, parentStyle) => {
    
            style.position = 'absolute';
            // style.marginTop = '0px';
            // style.height = '100%';
            
            // style.width = `${window.innerWidth}px`;
    
            parentStyle.alignItems = 'flex-end';
            parentStyle.justifyContent = 'flex-end';
    
          })
          // new GlobalPositionStrategy()
        });
        this.overlayRef = this.dynamicOverlay.create(config);
        this.toggler.subscribe(s=> {
          this.overlayRef.detach();
        });
        this.overlayRef.attach(loaderPortal);

    //    this.dynamicOverlay.create(attach(loaderPortal);
      } else {
        //  this.overlayRef.detach();
      }
    });
  }

  getInjector(data: LoaderData, parentInjector: Injector): PortalInjector {
    const tokens = new WeakMap();

    tokens.set(LoaderData, data);

    return new PortalInjector(parentInjector, tokens);
  }

  //setContainerElement
}