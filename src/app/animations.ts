import { trigger, transition, style, query, animateChild, group, animate, state, keyframes } from '@angular/animations';

export const slideInAnimation =
  trigger('routeAnimations', [
    transition('AboutPage => CoursePage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          opacity: 1
        })
      ]),
      query(':enter', [

        animateChild(),
        style({ opacity: 0,  left: '110%' }),

        animate('0.7s ease-out', style({ opacity: 1,  left: '0%' })),
      ], 
      { optional: true } ),
      query(':leave', [ 
        animateChild(),
          style({ opacity: 1, left: 0 }),
          
          animate('0.35s ease-out', style({ opacity: 0 /*, left: '110%'*/ })),
          // animateChild()
        ], 
        { optional: true }
      ),
     
      query(':enter', [ 
        // animateChild(),

        // style({ opacity: 0 /*, left: '110%' */}),
        animate('0.7s ease-out', style({ opacity: 1 /*, left: '0%' */})),
        
      ], 
      { optional: true })
      // ),
    ]
    // Child routes Faster White background hides
    
    ),
    transition('AboutPage <=> HomePage, HomePage <=> HomePage', [
      style({ position: 'relative', opacity: 1 }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          opacity: 1
        })
      ]),
      query(':enter', [
        animateChild(),
        style({ opacity: 0,  left: '0%' }), // 110%
        

        // on leave:
        // animate('1.7s ease-out', style({ 'left': '0%' })),

        animate('1.4s ease-out', style({ opacity: 1 })),
        // animate('1.7s ease-out', style({  left: '0%' })),
      ], 
      { optional: true } ),
      query(':leave', [ 
          style({ opacity: 1, left: 0 }),
          animateChild(),
          // animate('0.7s ease-out', style({ 'left': '110%' })),
      // on leave:
        // animate('1.7s ease-out', style({ 'left': '110%' })),
          animate('1.4s ease-out', style({ 'left': '110%', opacity: 0 /*, left: '110%'*/ })),
          // animateChild()
        ], 
        { optional: true }
      ),
     
      query(':enter', [ 
        // animateChild(),

        // style({ opacity: 0 /*, left: '110%' */}),
        animate('0.7s ease-out', style({ opacity: 1 /*, left: '0%' */})),
        
      ], 
      { optional: true })
      // ),
    ]
    // Child routes Faster White background hides
    
    ),
    transition('* => CoursePage1, * => CoursePage2, * => CoursePage3', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 'initial',
          width: '100%',
          'max-width': '1280px',
          opacity: 0.1,
          'padding-left': '20px'
        })
      ]),
      query(':enter', [

        animateChild(),
        style({ opacity: 0,  left: '110%' }),

        animate('0.6s ease-out', style({ opacity: 1,  left: '0%' })),
      ], 
      { optional: true } ),
      query(':leave', [ 
        animateChild(),
          style({ /*opacity: 1,*/ left: 0 }),
          
          // animate('0.1s', style({ opacity: 0 /*, left: '110%'*/ })),
          // animateChild()
        ], 
        { optional: true }
      ),
     
      query(':enter', [ 
        // animateChild(),

        // style({ opacity: 0 /*, left: '110%' */}),
        animate('0.6s ease', style({ opacity: 1 /*, left: '0%' */})),
        
      ], 
      { optional: true })
      // ),
    ])

//-----------------=================== 1 check
      // state('in', style({ opacity: 100 })),
   /*   query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '110%' }),
        style({ opacity: 0 })
      ]),
      // query(':leave', animateChild()),
      // group([
        query(':leave', [
          style({ opacity: 1 }),
          animate('0.92s', style({ opacity: 0 }))
        ]),
        //   style({ opacity: 100 }),
          
        // animateChild(),
        //   animate('1s', 
        //     keyframes([
        //   style({ opacity: 0 }),
        //   style({ left: '110%' })
          
        // ]),         
          // animate('1s cubic-bezier(.28,.11,.81,.47)',
        //   keyframes([
            
        //     style({ left: '0%' }),
        //     style({ left: '100%' })
        // ]
        // )), 
        
 
          
          // animate(1000, style({ opacity: 0 }))
        // ]),
        query(':enter', [
          // animateChild(),
          // style({ opacity: 100 }),
          // style({ left: '110%' }),
         
          //   animate('1.5s cubic-bezier(.38,.21,.81,.47)',
          //   keyframes([
          //     // style({ left: '200%' }),
          //     style({ left: '0%' })],
          //     // style({ opacity: 0 }),
          //     // style({ opacity: 100 }) ]

          //   )),

            style({ opacity: 0 }),
            animate('0.92s', style({ opacity: 1 }))
          // ], 
          // { optional: true }
        // )

          // animate('1s cubic-bezier(.28,.11,.81,.47)', keyframes( [
            // ))
          // animate('1s cubic-bezier(.28,.11,.81,.47)', style({ left: '0%' })),
          // animate(1000, style({ opacity: 100 }))
        
      ])
      // query(':enter', )
    ])*/,
    transition('* <=> FilterPage', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '-100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('200ms ease-out', style({ left: '100%' }))
        ]),
        query(':enter', [
          animate('300ms ease-out', style({ left: '0%' }))
        ])
      ]),
      query(':enter', animateChild()),
    ])
  ]);
