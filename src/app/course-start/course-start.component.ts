import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Service } from '../service.service';

@Component({
  selector: 'app-course-start',
  templateUrl: './course-start.component.html',
  styleUrls: ['./course-start.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CourseStartComponent implements OnInit {

  constructor(public service: Service) { }

  ngOnInit(): void {
  }

  
  navigateToSubscribe(str: string) {
    let formUrl = ''; 
    if (str === '' || str === undefined) {
       return;
    }
    switch (str) {
      case 'start': formUrl = 'https://forms.gle/GKqpNYnxJ2TPs7i68';
          break;
      case 'workshop': formUrl = 'https://forms.gle/kamEUECZzRUftUeg8';
          break;
      case 'special': formUrl = 'https://forms.gle/YVWyMvUNdAB2rqJC6';
            break;
    }
     window.open(formUrl);
   }
}
