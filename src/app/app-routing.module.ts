import {NgModule} from '@angular/core';
import {RouterModule, Routes, ExtraOptions} from '@angular/router';
import {FaqComponent} from './faq/faq.component';
import {HomeComponent} from './home/home.component';
import {FeedComponent} from './feed/feed.component';
import {HelloYouComponent} from './hello-you/hello-you.component';
import {BlogPostListingComponent} from './blog-post-listing/blog-post-listing.component';
import {CustomerListingComponent} from './customer-listing/customer-listing.component';
import {CustomerDetailsComponent} from './customer-details/customer-details.component';
import {BlogPostDetailsComponent} from './blog-post-details/blog-post-details.component';
import { environment } from 'src/environments/environment';
import { RealtorCourseDetailsComponent } from './realtor-course-details/realtor-course-details.component';
import { CourseStartComponent } from './course-start/course-start.component';
import { CourseWorkshopComponent } from './course-workshop/course-workshop.component';
import { CourseSpecialComponent } from './course-special/course-special.component';
const env = environment;
const appRoutes: Routes = [
    // {path: 'customer', component: CustomerListingComponent},
    // {path: 'customer/:slug', component: CustomerDetailsComponent},
    
    // {path: '', pathMatch: 'prefix', children: [ // environment.appRoot.length>0? environment.appRoot + '/' :
        {path:  'home', component: FaqComponent, data: {animation: 'HomePage'} },
        {path:  'blog', component: BlogPostListingComponent},
        {path:  'blog/:slug', component: BlogPostDetailsComponent},
        {path:  'rss', component: FeedComponent},
        // {path: env.appRoot + 'hello-you', component: HelloYouComponent},
        {path:  'faq', component: HomeComponent},
        //{path:  'realtors-course', pathMatch: 'full', redirectTo: 'realtors-course/workshop', data: {animation: 'AboutPage'} },
        // {   path:  'realtors-course/:id', pathMatch: 'prefix', data: {animation: 'AboutPage'},
            // children: [
                // { path: 'realtors-course', redirectTo: 'realtors-course/workshop', pathMatch: 'full' },
                {    path:  'realtors-course', pathMatch: 'full', redirectTo: 'realtors-course/workshop' },
                {    path:  'realtors-course', pathMatch: 'prefix', component: RealtorCourseDetailsComponent, 
                data: {animation: 'AboutPage'},
                    children: [
                        { path: '', redirectTo: 'realtors-course/workshop', pathMatch: 'full', data: {animation: 'CoursePage'} },
                        { path: 'start', component: CourseStartComponent, data: {animation: 'CoursePage1'} },
                        { path: 'workshop', component: CourseWorkshopComponent, data: {animation: 'CoursePage2'}  },
                        { path: 'special', component: CourseSpecialComponent, data: {animation: 'CoursePage3'}  }
                    ]
                },
            // ]
        // },
        {path: '**', redirectTo:  'home'}
    // ]},
    // {path: '**', redirectTo:  '/home'} // environment.appRoot.length >0 ? '/' + environment.appRoot +
    
];

const routerOptions: ExtraOptions = {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    // scrollOffset: [0, 64],
    scrollOffset: [0, 63],
    enableTracing: false,
    useHash: true
  };
  
@NgModule({
    imports: [RouterModule.forRoot(appRoutes, routerOptions)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
