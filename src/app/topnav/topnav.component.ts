import {Component, OnDestroy, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {Router, RouterOutlet} from '@angular/router';
import {Subscription} from 'rxjs';
import {Topnav, TopnavItem} from '../models';
import { MatMenuTrigger } from '@angular/material/menu';
import { environment } from 'src/environments/environment';
import { slideInAnimation } from '../animations';

@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss'],
    animations: [
        slideInAnimation
        // animation triggers go here
      ]
})
export class TopnavComponent implements OnInit, OnDestroy, AfterViewInit {
    
    topNav: Topnav;
    private subscription: Subscription;
    public rootUrl = '';
    isResponsive: boolean = false;
    @ViewChild('mainMenuTrigger') trigger: MatMenuTrigger;
    currentItemSelected: { [label: string]: boolean} = { 'Обучение': true }; // TopnavItem
    currentItem: TopnavItem;
 
    ngAfterViewInit() {
    //   setTimeout(() => {
    //     this.trigger.openMenu(); // open the main menu
    //   }, 0);
        
    }

    
    prepareRoute(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
    }
  
    constructor(private router: Router) {
        if (window.outerWidth < 1000) {
            this.isResponsive = true;
        }
        this.currentItemSelected =  { 'Обучение': true };
    }

    ngOnInit() {
        if (window.outerWidth < 1000) {
             this.isResponsive = true;
        }
        this.createTopNavItems();
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    // isSelectedItem(item: TopnavItem): boolean {
    //     return this.currentItemSelected === item;
    // }
/*
    set config(setItem: { [label: string]: boolean}) { // TopnavItem
        this.currentItemSelected =  setItem; // { [ setItem.textContent]: true };
    }
*/
    public get config(): boolean /*{ [label: string]: boolean }*/ { 
        // this.topNav.commonItems.filter( returnValue => returnValue === this.currentItemSelected)[0].textContent]
        //if(this.topNav.commonItems.filter( returnValue => { return this.currentItemSelected[returnValue.textContent]===undefined || this.currentItemSelected[returnValue.textContent]===false}).length === 0) {
        
            if(this.currentItemSelected===undefined || this.currentItemSelected[this.currentItem.textContent] === false) {
        // if(this.topNav.commonItems.filter( returnValue => { return item === returnValue.textContent }).length === 0) { 
        
        //   let item = this.topNav.commonItems.filter( returnValue => { return this.currentItemSelected[returnValue.textContent]!==undefined&&  this.currentItemSelected[returnValue.textContent]!==false})[0];
        
        return false;// this.currentItemSelected[item.textContent].valueOf();
        }
        else 
        {
            return true; //this.currentItemSelected;
        }
      }

      public set config(value: boolean) {
        this.currentItemSelected[this.currentItem.textContent] = value;
      }
      private getPromise(thisObj: any, item: any) {
        return new Promise( (resolve) => {
            const currentItem = item;
            Object.keys(thisObj.currentItemSelected).forEach(key => {
                this.currentItemSelected[key] = false;
            }, thisObj );
            resolve(currentItem);
        });
      }

    private createTopNavItems() {
        const thisObj: TopnavComponent = this;
        this.topNav = {
            home: [
                {
                    textContent: 'Обучение',
                    icon: 'home',
                    svg: true,
                    click: (item: TopnavItem) => { 

                        const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                            thisObj.currentItemSelected[ itemkey ] = true;
                            // this.currentItem = item;
                            // this.config[item.textContent] = true;
                            thisObj.gotoHome();
                        });
                    }
                }
            ],
            commonItems: [
                {
                    textContent: 'Обучение',
                    icon: 'home',
                    svg: true,
                   click: (item: TopnavItem) => {
                    const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                            thisObj.currentItemSelected[itemkey] = true;
                            // this.currentItem = item;
                            // this.config[item.textContent] = true;
                            thisObj.gotoHome();
                        });
                    }
                },
                {
                    textContent: 'Практикум партнерства',
                    icon: undefined,
                   click: (item: TopnavItem) => { 
                    const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                        thisObj.currentItemSelected[itemkey] = true;
                        thisObj.router.navigate(['realtors-course', 'workshop'], { fragment: 'scrollOperational', replaceUrl: true, skipLocationChange: false });
                    });
                    }

                         // this.gotoCustomer()
                    // this.router.navigateByUrl('/' + environment.appRoot +
                },
                {
                    textContent: 'Курс "R-Start. Как стать риэлтором"',
                    icon: undefined,
                   click: (item: TopnavItem) => { 
                    const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                        thisObj.currentItemSelected[itemkey] = true;
                        thisObj.router.navigate(['realtors-course', 'start'], { fragment: 'scrollOperational', replaceUrl: true, skipLocationChange: false }); // this.gotoCustomer()
                        });
                    }
                    // this.router.navigateByUrl('/' + environment.appRoot +
                    // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + 
                        // '/realtors-course/start#scrollOperational')
                },
                {
                    textContent: 'Специальные программы',
                    icon: undefined,
                   click: (item: TopnavItem) => {
                    const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                        thisObj.currentItemSelected[itemkey] = true;
                        thisObj.router.navigate(['realtors-course', 'special'], { fragment: 'scrollOperational', replaceUrl: true, skipLocationChange: false });
                    });


                         // thisObj.gotoCustomer()
                    }
                    // this.router.navigateByUrl('/' + environment.appRoot +
                    // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + 
                    // '/realtors-course/special#scrollOperational')
                },
                {
                    textContent: 'Отзывы (участников)',
                    icon: 'forum',
                   click: (item: TopnavItem) => {
                    const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                        thisObj.currentItemSelected[itemkey] = true;
                        thisObj.router.navigate(['home'], { fragment: 'Testimonials', replaceUrl: true, skipLocationChange: false }); 
                    });
                        // this.gotoCustomer()
                    }
                    // this.router.navigateByUrl('/' + environment.appRoot +
                    // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + '/home#Testimonials')
                },
                {
                    textContent: 'Контакты',
                    icon: 'person',
                   click: (item: TopnavItem) => { 
                    const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
                        thisObj.currentItemSelected[ itemkey ] = true;
                        thisObj.router.navigate(['home'], { fragment: 'Contacts', replaceUrl: true, skipLocationChange: false });
                    });
                        
                    //}); 
                        // this.currentItemSelected[itemkey] = true;
                        // this.router.navigate(['home'], { fragment: 'Contacts' }) // this.gotoCustomer()
                    }
                    // this.router.navigateByUrl('/' + environment.appRoot +
                        // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + '/home#Contacts')
                } /*,
                {
                    textContent: 'Сертификаты',
                    icon: undefined,
                    click: () => this.gotoMisc()
                } *//*,
                {
                    textContent: 'RSS, Atom & Sitemap',
                    icon: undefined,
                    click: () => this.gotoMisc()
                }*/
            ]
        };
    }

    public navigateToRouterLink(url: string) {
        // this.router.navigate([ (environment.appRoot.length > 0 ? '/' + environment.appRoot : '/' ) + url /*`home`*/ ]);
        this.router.navigate([ url /*`home`*/ ], { replaceUrl: true, skipLocationChange: false });
    }
    gotoHome() {
       this.navigateToRouterLink(`home/`);
    }

    gotoCustomer() {
        this.navigateToRouterLink(`customer`);
    }

    gotoFaq() {
        this.navigateToRouterLink(`faq`);
    }

    gotoBlogPosts() {
        this.navigateToRouterLink(`blog`);
    }

    gotoMisc() {
        this.navigateToRouterLink(`rss`);
    }
}

