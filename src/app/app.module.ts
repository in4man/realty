import {BrowserModule} from '@angular/platform-browser';
import {NgModule, ɵɵinject, ErrorHandler} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HelloYouComponent} from './hello-you/hello-you.component';
import {BlogPostDetailsComponent} from './blog-post-details/blog-post-details.component';
import {BlogPostListingComponent} from './blog-post-listing/blog-post-listing.component';
import {CustomerDetailsComponent} from './customer-details/customer-details.component';
import {CustomerListingComponent} from './customer-listing/customer-listing.component';
import {FaqComponent} from './faq/faq.component';
import {FeedComponent} from './feed/feed.component';
import {TopnavComponent} from './topnav/topnav.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AppMaterialModule} from './shared';
import {HomeComponent} from './home/home.component';
import { SafePipe } from './safe.pipe';
import { APP_BASE_HREF, DOCUMENT, ViewportScroller, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { environment } from 'src/environments/environment';
import { RealtorCourseDetailsComponent } from './realtor-course-details/realtor-course-details.component';
import { MatCarouselModule } from './lib/carousel/carousel.module';
import { LoaderComponent } from './lib/loader/loader.component';
import { DynamicOverlay } from './lib/loader/dynamic-overlay.service';
import { OverlayLoadingDirective } from './directives/overlay-loading.directive';
import { OverlayModule } from '@angular/cdk/overlay';
import { LoaderData } from './lib/loader/loader.config';
import { NgImageSliderModule } from './lib/ng-slider/ng-image-slider.module';
import { NgImageSliderComponent } from './lib/ng-slider/ng-image-slider.component';
import { CustomViewportScroller } from './lib/custom-scroller/custom-viewport-scroller';
import { CourseStartComponent } from './course-start/course-start.component';
import { CourseWorkshopComponent } from './course-workshop/course-workshop.component';
import { CourseSpecialComponent } from './course-special/course-special.component';
@NgModule({
    declarations: [
        AppComponent,
        HelloYouComponent,
        BlogPostDetailsComponent,
        BlogPostListingComponent,
        CustomerDetailsComponent,
        CustomerListingComponent,
        RealtorCourseDetailsComponent,
        FaqComponent,
        FeedComponent,
        HomeComponent,
        TopnavComponent,
        SafePipe,
        LoaderComponent,
        OverlayLoadingDirective,
        CourseStartComponent,
        CourseWorkshopComponent,
        CourseSpecialComponent
        ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FlexLayoutModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppMaterialModule,
        OverlayModule,
        MatCarouselModule.forRoot(),
        NgImageSliderModule
    ],
    providers: [ LoaderData, DynamicOverlay,
        {
            provide: ViewportScroller,
            useFactory: () => new CustomViewportScroller('scrollOperational', ɵɵinject(DOCUMENT), window, ɵɵinject(ErrorHandler))
          },
        //   {
        //     provide: LocationStrategy,
        //     useClass:  HashLocationStrategy
        //   },
        {provide: APP_BASE_HREF, useValue: environment.appRoot.length > 0 ? environment.appRoot + '/': '/' }],
    bootstrap: [AppComponent],
    entryComponents: [ LoaderComponent ]
})
export class AppModule {
}
