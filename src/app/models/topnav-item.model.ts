export interface TopnavItem {
    icon: string;
    svg?: boolean;
    textContent: string;
    click: (sender: TopnavItem) => void;
    isSubmenuHeader?: boolean;
    subItems?: TopnavItem[];
}
