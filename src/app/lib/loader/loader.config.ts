import { Injectable } from '@angular/core';

@Injectable()
export class LoaderData {
  title: string;
  loaderType: LoaderType;
}

export type LoaderType = 'Start' | 'Special' | 'Praktikum';
