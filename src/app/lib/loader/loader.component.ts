import { Component, Output, EventEmitter } from '@angular/core';

import { LoaderData } from './loader.config';
import { OverlayRef } from '@angular/cdk/overlay';
import { Service } from 'src/app/service.service';

@Component({
  selector: 'app-loader',
  template: `
  

  <div style="width: 100%; color: black; padding-left: 2%; display: flex;
    justify-items: center;
    align-items: end;
    align-content: end;
    justify-content: center;" *ngIf="data !== undefined">
  <div *ngIf="data.loaderType !== ''" class="menu_nav_pages" (click)="close()" style="
      width: 75%; margin-top: 0.6%;
    color: black;
    padding-left: 2%;
    position: absolute;
    margin-left: auto;
    margin-right: 0.8% ;
    display: flex;
    justify-items: center;
    align-items: end;
    align-content: end;
    flex-direction: row-reverse;    
    justify-content: end;
    
    font-family: Tahoma; cursor: pointer;
  
   z-index: 99991;
    color: black;
    /* padding-left: 2%; */
    /* position: absolute; */
 
    flex-direction: row-reverse;">
 <div  style="text-align: right"> X</div>
  </div>
  <ng-content select=".Praktikum"></ng-content>
  <ng-content select=".Start"></ng-content>
  <ng-content select=".Special"></ng-content>  
  <!-- <mat-card>
      <mat-card-header>
        <mat-card-title>
          {{ data.title }}
        </mat-card-title>
      </mat-card-header>
      <mat-card-content>
	  TEST WORKS - {{ data.title }}
        < !--<mat-progress-bar *ngIf="isProgressBar" mode="query"></mat-progress-bar>
        <mat-progress-spinner
          *ngIf="isSpinner"
          mode="indeterminate"
        ></mat-progress-spinner>-- >
      </mat-card-content>
    </mat-card> -->

  </div>
 `,
    styles: [`:host { 
      position: absolute;
      top: 0;   
      /* bottom: 0; */
      z-index: 9999;
      /* width: 77%; */ /* fullscreen loader*/
      padding-right: 2vw;
      /* background-color: white; */
      height: 100%;
      overflow-y: visible;
      pointer-events: all !important;
      top: 0;
      left: 0;
      
    }

    }
    /* .Praktikum li {
          font-weight: bold;
    } */
    :host > div { border-radius: 3px; background-color: rgba(220, 220, 220, 0.868); 
      -webkit-box-shadow: 7px 7px 2px -2px rgba(140,140,140,0.8);
      -moz-box-shadow: 7px 7px 2px -2px rgba(140,140,140,0.8);
      box-shadow: 7px 7px 2px -2px rgba(140,140,140,0.8);
  }`]
})
export class LoaderComponent {

  @Output('closePanel') closePanel = new EventEmitter<boolean>();
  // private overlayRef: OverlayRef;
  close() {
    this.closePanel.emit(false);
    this.service.subscription.emit(false);
    // this.overlayRef.detach();
  }

  // private overlayRef: OverlayRef;
  // setOverlayRef(ref) {
  //   this.overlayRef = ref;
  // }
  constructor(public readonly data: LoaderData, public service: Service) {}

    get isStartCourse(): boolean {
      return this.data.loaderType === 'Start';
    }
  
    get isPractikumCourse(): boolean {
      return this.data.loaderType === 'Praktikum';
    }

    get isSpecialCourse(): boolean {
      return this.data.loaderType === 'Special';
    }
    
//   get isSpinner(): boolean {
//     return this.data.loaderType === 'Spinner';
//   }

//   get isProgressBar(): boolean {
//     return this.data.loaderType === 'ProgressBar';
//   }
}
