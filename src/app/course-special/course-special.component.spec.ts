import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseSpecialComponent } from './course-special.component';

describe('CourseSpecialComponent', () => {
  let component: CourseSpecialComponent;
  let fixture: ComponentFixture<CourseSpecialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseSpecialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseSpecialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
