import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Service } from '../service.service';

@Component({
  selector: 'app-course-workshop',
  templateUrl: './course-workshop.component.html',
  styleUrls: ['./course-workshop.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CourseWorkshopComponent implements OnInit {

  constructor(public service: Service) { }

  ngOnInit(): void {
  }

  
  navigateToSubscribe(str: string) {
    let formUrl = ''; 
    if (str === '' || str === undefined) {
       return;
    }
    switch (str) {
      case 'start': formUrl = 'https://forms.gle/GKqpNYnxJ2TPs7i68';
          break;
      case 'workshop': formUrl = 'https://forms.gle/kamEUECZzRUftUeg8';
          break;
      case 'special': formUrl = 'https://forms.gle/YVWyMvUNdAB2rqJC6';
            break;
    }
     window.open(formUrl);
   }

}
