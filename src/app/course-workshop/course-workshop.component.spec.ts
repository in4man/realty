import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseWorkshopComponent } from './course-workshop.component';

describe('CourseWorkshopComponent', () => {
  let component: CourseWorkshopComponent;
  let fixture: ComponentFixture<CourseWorkshopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseWorkshopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
