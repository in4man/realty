
import { PositionStrategy, OverlayRef, OverlayConfig } from '@angular/cdk/overlay';

export class ResponsivePositionStrategy implements PositionStrategy {

  private overlayRef: OverlayRef;

  constructor(
    private updater: (config: OverlayConfig, style: CSSStyleDeclaration, parentStyle: CSSStyleDeclaration) => void
  ) { }

  public attach(overlayRef: OverlayRef): void {
    this.overlayRef = overlayRef;
    overlayRef.hostElement.classList.add('cdk-global-overlay-wrapper');
  }

  public apply(): void {
    if (!this.overlayRef.hasAttached()) {
      return;
    }

    const styles = this.overlayRef.overlayElement.style;
    const parentStyles = this.overlayRef.hostElement.style;
    const config = this.overlayRef.getConfig();

    if (config.width === '100%') {
      parentStyles.justifyContent = 'flex-start';
    }

    // if (config.height === '100%') {
      parentStyles.alignItems = 'flex-start';
    // }

    this.updater(config, styles, parentStyles);

  }

  public dispose(): void { }

}
