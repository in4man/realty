import {Component, OnInit, HostListener, ViewChild, ElementRef, AfterContentInit, AfterViewInit} from '@angular/core';
import {butterService} from '../services';
import { environment } from '../../environments/environment';
import { ViewportScroller } from '@angular/common';
import { RouterOutlet, ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { slideInAnimation } from '../animations';
import { Service } from '../service.service';
import { ViewEncapsulation } from '@angular/compiler/src/compiler_facade_interface';
import { Topnav, TopnavItem } from '../models';

@Component({
    selector: 'app-realtor-course-details',
    templateUrl: './realtor-course-details.component.html',
    styleUrls: ['./realtor-course-details.component.scss'],
    animations: [
      slideInAnimation
      // animation triggers go here
    ]
})
export class RealtorCourseDetailsComponent implements OnInit, AfterContentInit, AfterViewInit {

    public env: any = environment;
    sideNav: Topnav;
    /*
    @ViewChild('FAQ_1') private reg1: ElementRef<HTMLBaseElement>;
    @ViewChild('FAQ_2') private reg2: ElementRef<HTMLBaseElement>;

    @ViewChild('FAQ_3') private reg3: ElementRef<HTMLBaseElement>;
    @ViewChild('FAQ_4') private reg4: ElementRef<HTMLBaseElement>;
*/
    @ViewChild('region_nav1') private regnav1: ElementRef<HTMLBaseElement>;
    @ViewChild('region_nav2') private regnav2: ElementRef<HTMLBaseElement>;

    @ViewChild('region_nav3') private regnav3: ElementRef<HTMLBaseElement>;
    @ViewChild('region_nav4') private regnav4: ElementRef<HTMLBaseElement>;

    fixedBoxOffsetTop: number  = 0;

    @ViewChild('fixedBox') fixedBox: ElementRef;
    
    isResponsive: boolean = false;

    constructor(private router: Router, private viewportScroller: ViewportScroller, public service: Service, private ars: ActivatedRoute) {
      if (window.outerWidth < 1000) {
        this.isResponsive = true;
    }
    // this.currentItemSelected =  { 'Обучение': true };
    }

    private createTopNavItems() {
      const thisObj: RealtorCourseDetailsComponent = this;
      // this.sideNav = {
      //     home: [
      //         {
      //             textContent: 'Обучение',
      //             icon: 'home',
      //             svg: true,
      //             click: (item: TopnavItem) => { 

      //                 const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                     thisObj.currentItemSelected[ itemkey ] = true;
      //                     // this.currentItem = item;
      //                     // this.config[item.textContent] = true;
      //                     thisObj.gotoHome();
      //                 });
      //             }
      //         }
      //     ],
      //     commonItems: [
      //         {
      //             textContent: 'Обучение',
      //             icon: 'home',
      //             svg: true,
      //            click: (item: TopnavItem) => {
      //             const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                     thisObj.currentItemSelected[itemkey] = true;
      //                     // this.currentItem = item;
      //                     // this.config[item.textContent] = true;
      //                     thisObj.gotoHome();
      //                 });
      //             }
      //         },
      //         {
      //             textContent: 'Практикум партнерства',
      //             icon: undefined,
      //            click: (item: TopnavItem) => { 
      //             const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                 thisObj.currentItemSelected[itemkey] = true;
      //                 thisObj.router.navigate(['realtors-course', 'workshop'], { fragment: 'scrollOperational', replaceUrl: true, skipLocationChange: false });
      //             });
      //             }

      //                  // this.gotoCustomer()
      //             // this.router.navigateByUrl('/' + environment.appRoot +
      //         },
      //         {
      //             textContent: 'Курс "R-Start. Как стать риэлтором"',
      //             icon: undefined,
      //            click: (item: TopnavItem) => { 
      //             const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                 thisObj.currentItemSelected[itemkey] = true;
      //                 thisObj.router.navigate(['realtors-course', 'start'], { fragment: 'scrollOperational', replaceUrl: true, skipLocationChange: false }); // this.gotoCustomer()
      //                 });
      //             }
      //             // this.router.navigateByUrl('/' + environment.appRoot +
      //             // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + 
      //                 // '/realtors-course/start#scrollOperational')
      //         },
      //         {
      //             textContent: 'Специальные программы',
      //             icon: undefined,
      //            click: (item: TopnavItem) => {
      //             const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                 thisObj.currentItemSelected[itemkey] = true;
      //                 thisObj.router.navigate(['realtors-course', 'special'], { fragment: 'scrollOperational', replaceUrl: true, skipLocationChange: false });
      //             });


      //                  // thisObj.gotoCustomer()
      //             }
      //             // this.router.navigateByUrl('/' + environment.appRoot +
      //             // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + 
      //             // '/realtors-course/special#scrollOperational')
      //         },
      //         {
      //             textContent: 'Отзывы (участников)',
      //             icon: 'forum',
      //            click: (item: TopnavItem) => {
      //             const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                 thisObj.currentItemSelected[itemkey] = true;
      //                 thisObj.router.navigate(['home'], { fragment: 'Testimonials', replaceUrl: true, skipLocationChange: false }); 
      //             });
      //                 // this.gotoCustomer()
      //             }
      //             // this.router.navigateByUrl('/' + environment.appRoot +
      //             // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + '/home#Testimonials')
      //         },
      //         {
      //             textContent: 'Контакты',
      //             icon: 'person',
      //            click: (item: TopnavItem) => { 
      //             const resolver = this.getPromise(thisObj, item).then( (itemkey: any) => {
      //                 thisObj.currentItemSelected[ itemkey ] = true;
      //                 thisObj.router.navigate(['home'], { fragment: 'Contacts', replaceUrl: true, skipLocationChange: false });
      //             });
                      
      //             //}); 
      //                 // this.currentItemSelected[itemkey] = true;
      //                 // this.router.navigate(['home'], { fragment: 'Contacts' }) // this.gotoCustomer()
      //             }
      //             // this.router.navigateByUrl('/' + environment.appRoot +
      //                 // (environment.appRoot.length > 0 ? '/#' : '') + environment.appRoot  + '/home#Contacts')
      //         } /*,
      //         {
      //             textContent: 'Сертификаты',
      //             icon: undefined,
      //             click: () => this.gotoMisc()
      //         } *//*,
      //         {
      //             textContent: 'RSS, Atom & Sitemap',
      //             icon: undefined,
      //             click: () => this.gotoMisc()
      //         }*/
      //     ]
      // };
  }

    public getLightRegionIsActive(j: number): boolean {
      if (this.regnav1 === undefined) {
          return false;
      }

      const params: ElementRef[] = [ this.regnav1, this.regnav2, this.regnav3 ];
      const i = j - 1;
      return params[i].nativeElement.classList.contains('w--current');
    }

    public lightRegion(j: number, /*...*/params: ElementRef[]): void {
      const i = j - 1;
      if ( !(params[i].nativeElement.classList.contains('w--current')) ) {

        for(let k=0; k < params.length; k++) {
          if (params[k].nativeElement.classList.contains('w--current') && i !== k && params[i].nativeElement.classList.contains('w--current') ) {
           params[k].nativeElement.classList.remove('w--current');
          }
        }

        params[i].nativeElement.classList.add('w--current');
     
        
       
      } else {
        // for(let k=0; k < params.length; k++) {
        //   if (params[k].nativeElement.classList.contains('w--current')) {
        //    params[k].nativeElement.classList.remove('w--current');
        //   }
        // }        
      }
    }
    
    public scrollTo(str: string) {
      // first part: setOffset in onInit
console.log(this.viewportScroller.getScrollPosition()); 
  
      // this.viewportScroller.scrollToPosition([0, this.fixedBoxOffsetTop + this.reg2.nativeElement.getBoundingClientRect().top ]);

      this.viewportScroller.scrollToAnchor(str);
    }
    // @HostListener('window:scroll', ['$event']) // for window scroll events
    // @HostListener('scroll')
    // onScroll(event) {
      onMenuNavigated() {


      //main part:

      const lightRegionFunction = this.lightRegion;
      // const regnav1 = this.regnav1,regnav2 = this.regnav2, regnav3 = this.regnav3, regnav4 = this.regnav4;



      // lightRegionFunction(Number(entry[0].target.getAttribute('id').replace('FAQ_','')), regnav1,regnav2,regnav3,regnav4);
         
    /*  var observer = new IntersectionObserver(function(entry, observer) {
        // isIntersecting is true when element and viewport are overlapping
        // isIntersecting is false when element and viewport don't overlap
        
        // for(let i=0; i<entry[0].length; i++) {
          if(entry[0].isIntersecting === true) {
            console.log('Element has just become visible in screen');
            lightRegionFunction(Number(entry[0].target.getAttribute('id').replace('FAQ_','')), regnav1,regnav2,regnav3,regnav4);
          }
        // }
      }, { threshold: [0.30] });
      
      observer.observe(
        this.reg1.nativeElement
      );
      observer.observe(
        this.reg2.nativeElement
      );
      observer.observe(
        this.reg3.nativeElement
      );
      observer.observe(
        this.reg4.nativeElement
      ); */
       
    }

    public home_page_content: any = {
        advantages_of_our_approach: [],
        title: 'intro-description',
        pictures_in_front: '',
        welcome: ''
    };

    
  public ngAfterViewInit(): void {
    // this.ars.fragment.subscribe( sbs => {
      const ars = this.ars;
    setTimeout( () => {
      if (this.router.url && this.router.url.toString() !== '' ) {
        // this.router.navigate(['home'], { fragment: this.ars.snapshot.fragment });

        const url = this.router.url.toString(); // this.ars.snapshot.url.toString();
        const array: ElementRef[] = [this.regnav1, this.regnav2, this.regnav3];  /*, this.regnav4 */
        // switch (event.url.toString().substr(event.url.toString().lastIndexOf('/'))) {
        // switch (
        //   (url.toString().lastIndexOf('#') > url.toString().lastIndexOf('/') ?
        //   url.toString().substr(url.toString().lastIndexOf('/'),
        //     url.toString().lastIndexOf('#') - url.toString().lastIndexOf('/') ) :
        //       url.toString().substr(url.toString().lastIndexOf('/')))
        let courseId = ars.snapshot.paramMap.get('id');
            
        if ( courseId == undefined || courseId.length == 0) { courseId = 'workshop'; }

        // switch ( courseId ) {
        //   // count of symbols to ending "#" in url
        //   case 'start': this.lightRegion(2, array); break;
        //   case 'workshop': this.lightRegion(1, array); break;
        //   case 'special': this.lightRegion(3, array); break;
        //   default: break;
        // }
        console.log(event);
      }
  }); 

  }

    /* 
    "fields": {
    "seo": {
      "title": "Маргарита",
      "meta_description": "Профессиональные услуги, консалтинг и тренер по курсу \"Агенства недвижимости\""
    },
    "": "<ul>\n<li>Номер 1</li>\n<li>Номер два</li>\n<li><iframe width=\"560\" height=\"314\" src=\"//www.youtube.com/embed/wtUr3-cv68g\" allowfullscreen=\"allowfullscreen\"></iframe></li>\n</ul>",
    "pictures_in_front": "https://cdn.buttercms.com/l1hzCk8PSxSTCLLIsmmV",
    "advantages_of_our_approach": [
      {
        "text_for_services": "<h1 class=\"j-blog-header j-blog-headline j-blog-post--headline\">Недвижимость без комиссии</h1>\n<ul>\n<li class=\"j-blog-header j-blog-headline j-blog-post--headline\">описание</li>\n<li class=\"j-blog-header j-blog-headline j-blog-post--headline\">Статьи по теме:</li>\n<li class=\"j-blog-header j-blog-headline j-blog-post--headline\">12 шагов успешной продажи недвижимости. Как происходит продажа квартиры на вторичном рынке?</li>\n</ul>",
        "service_picture": "https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s107259a18929bec5/image/ia60c28b1ecd08973/version/1583923051/image.jpg"
      },
      {
        "text_for_services": "<ul>\n<li>\n<h2><strong>2. Почему я работаю только по договору</strong><strong></strong></h2>\n</li>\n<li><strong>Описание</strong></li>\n</ul>",
        "service_picture": "https://image.jimcdn.com/app/cms/image/transf/dimension=434x1024:format=jpg/path/s107259a18929bec5/image/i437fe9cfcee392a0/version/1537654174/image.jpg"
      },
      {
        "text_for_services": "<h1 class=\"\" id=\"cc-m-header-9164896098\"><img src=\"https://cdn.buttercms.com/w1HskmusTaeVsU9B8XAw\" alt=\"undefined\" class=\"butter-float-right\" /></h1>\n<h2 class=\"j-blog-headline\">1. Услуга для продавца и покупающего недвижимость.</h2>\n<p>В чем наши преимущества:) ?</p>\n<p>Вот пару заголовков из статей, в которых есть много интересного (про продажу и покупку квартир и домов по нашей оптимальной и прогрессивной системе продаж, признаваемой международно.)</p>\n<h2 class=\"j-blog-headline\">В чьих интересах работают агенты, которые рекламируют Вашу квартиру?</h2>\n<p><a href=\"https://www.myrealtykiev.com/2020/07/01/%D0%B2-%D1%87%D1%8C%D0%B8%D1%85-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%B0%D1%85-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D1%8E%D1%82-%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D1%8B-%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5-%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D0%B8%D1%80%D1%83%D1%8E%D1%82-%D0%B2%D0%B0%D1%88%D1%83-%D0%BA%D0%B2%D0%B0%D1%80%D1%82%D0%B8%D1%80%D1%83/\" class=\"j-blog-header\"></a><span></span><time class=\"datetime\" datetime=\"2020-07-01\"><span class=\"day \">01</span><span>&nbsp;</span><span class=\"mon \">Jul</span><span>&nbsp;</span><span class=\"yr \">202</span></time></p>\n<h1 class=\"\" id=\"cc-m-header-9164896098\">Все предлагаемые к продаже объекты - без комиссии с покупателя!</h1>",
        "service_picture": "https://cdn.buttercms.com/mUVZ4WfQlyj9xIMPheho"
      }
    ]
  }
  */

 
  ngAfterContentInit(): void {

    setTimeout( () => {
    const rect = this.fixedBox.nativeElement.getBoundingClientRect();
    this.fixedBoxOffsetTop = rect.top + rect.height + window.pageYOffset - document.documentElement.clientTop;
     
    this.viewportScroller.setOffset( [window.pageXOffset + rect.left, this.fixedBoxOffsetTop ]);

    // const array: ElementRef[] = [this.regnav1, this.regnav2, this.regnav3];  /*, this.regnav4 */
    // switch (this.router.url.toString().substr(this.router.url.toString().lastIndexOf('/'))) {
    //   case '/start': this.lightRegion(2, array); break;
    //   case '/workshop': this.lightRegion(1, array); break;
    //   case '/special': this.lightRegion(3, array); break;
    //   default: break;
    // }
    
    }, 0);
    // this.viewportScroller.scrollToAnchor(str);
    
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

    ngOnInit() {
        // butterService.page.retrieve('page_home', 'home')
        //  courseId = this.ars.snapshot.paramMap.get('id');
        this.createTopNavItems();

        const ars = this.ars;
        this.router.events.pipe(filter(event => event instanceof NavigationEnd))
        .subscribe( (event : NavigationEnd) => 
         {
           const array: ElementRef[] = [this.regnav1, this.regnav2, this.regnav3];  /*, this.regnav4 */
            // switch (event.url.toString().substr(event.url.toString().lastIndexOf('/'))) {
            
            /* switch (
              (event.url.toString().lastIndexOf('#') > event.url.toString().lastIndexOf('/') ?
              event.url.toString().substr(event.url.toString().lastIndexOf('/'), 
                event.url.toString().lastIndexOf('#') - event.url.toString().lastIndexOf('/') ) :
                  event.url.toString().substr(event.url.toString().lastIndexOf('/')))
              // count of symbols to ending "#" in url
            ) */ 
            let courseId = ars.snapshot.paramMap.get('id');
            
            if ( courseId == undefined || courseId.length == 0) { courseId = 'workshop'; }

            // switch ( courseId ) {
            //   case 'start': this.lightRegion(2, array); break;
            //   case 'workshop': this.lightRegion(1, array); break;
            //   case 'special': this.lightRegion(3, array); break;
            //   default: break;
            // }
            console.log(event);
         });
       
        // .then((res: any) => {
            const res: any =  { 'fields': {
                      'seo': {
                        'title': 'Маргарита',
                        'meta_description': 'Профессиональные услуги, консалтинг и тренер по курсу "Агенства недвижимости"'
                      },
                      'welcome': '<ul>\n<li>Номер 1</li>\n<li>Номер два</li>\n<li><iframe width="560" height="314" src="//www.youtube.com/embed/wtUr3-cv68g" allowfullscreen="allowfullscreen"></iframe></li>\n</ul>',
                      'pictures_in_front': 'https://cdn.buttercms.com/l1hzCk8PSxSTCLLIsmmV',
                      'advantages_of_our_approach': [
                        {
                          'text_for_services': '<h1 class="j-blog-header j-blog-headline j-blog-post--headline">Недвижимость без комиссии</h1>\n<ul>\n<li class="j-blog-header j-blog-headline j-blog-post--headline">описание</li>\n<li class="j-blog-header j-blog-headline j-blog-post--headline">Статьи по теме:</li>\n<li class="j-blog-header j-blog-headline j-blog-post--headline">12 шагов успешной продажи недвижимости. Как происходит продажа квартиры на вторичном рынке?</li>\n</ul>',
                          'service_picture': 'https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s107259a18929bec5/image/ia60c28b1ecd08973/version/1583923051/image.jpg'
                        },
                        {
                          'text_for_services': '<ul>\n<li>\n<h2><strong>2. Почему я работаю только по договору</strong><strong></strong></h2>\n</li>\n<li><strong>Описание</strong></li>\n</ul>',
                          'service_picture': 'https://image.jimcdn.com/app/cms/image/transf/dimension=434x1024:format=jpg/path/s107259a18929bec5/image/i437fe9cfcee392a0/version/1537654174/image.jpg'
                        },
                        {
                          'text_for_services': '<h1 class="" id="cc-m-header-9164896098"><img src="https://cdn.buttercms.com/w1HskmusTaeVsU9B8XAw" alt="undefined" class="butter-float-right" /></h1>\n<h2 class="j-blog-headline">1. Услуга для продавца и покупающего недвижимость.</h2>\n<p>В чем наши преимущества:) ?</p>\n<p>Вот пару заголовков из статей, в которых есть много интересного (про продажу и покупку квартир и домов по нашей оптимальной и прогрессивной системе продаж, признаваемой международно.)</p>\n<h2 class="j-blog-headline">В чьих интересах работают агенты, которые рекламируют Вашу квартиру?</h2>\n<p><a href="https://www.myrealtykiev.com/2020/07/01/%D0%B2-%D1%87%D1%8C%D0%B8%D1%85-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%B0%D1%85-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D1%8E%D1%82-%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D1%8B-%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5-%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D0%B8%D1%80%D1%83%D1%8E%D1%82-%D0%B2%D0%B0%D1%88%D1%83-%D0%BA%D0%B2%D0%B0%D1%80%D1%82%D0%B8%D1%80%D1%83/" class="j-blog-header"></a><span></span><time class="datetime" datetime="2020-07-01"><span class="day ">01</span><span>&nbsp;</span><span class="mon ">Jul</span><span>&nbsp;</span><span class="yr ">202</span></time></p>\n<h1 class="" id="cc-m-header-9164896098">Все предлагаемые к продаже объекты - без комиссии с покупателя!</h1>',
                          'service_picture': 'https://cdn.buttercms.com/mUVZ4WfQlyj9xIMPheho'
                        }
                      ]
                    }
              };
              const result = res;

              if (result.fields !== undefined) {
                this.home_page_content.title = result.fields.seo.title; //.faq_headline;
                this.home_page_content.welcome = result.fields.welcome; //.faq_headline;

                this.home_page_content.pictures_in_front = result.fields['pictures_in_front'];
                this.home_page_content.advantages_of_our_approach = result.fields['advantages_of_our_approach'];
              } // ).catch(f=>console.log(f));
            //   if(result.data && result.data.fields !== undefined)
            //     this.home_page_content.title = result.data.fields.seo.title; //.faq_headline;
            //     this.home_page_content.welcome = result.data.fields.welcome; //.faq_headline;
                
            //     this.home_page_content.pictures_in_front = result.data.fields['pictures_in_front'];
            //     this.home_page_content.advantages_of_our_approach = result.data.fields['advantages_of_our_approach'];
            // }).catch(f=>console.log(f));

    }
}
