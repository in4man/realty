import {Component, OnInit, ElementRef, ViewChild, HostListener, AfterViewChecked, AfterViewInit } from '@angular/core';
import {butterService} from '../services';
import { environment } from '../../environments/environment';
import { Router, NavigationEnd, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { LoaderComponent } from '../lib/loader/loader.component';
import { Observable, BehaviorSubject, of, from } from 'rxjs';
import { EventEmitter } from '@angular/core';


import { Overlay, OverlayRef, OverlayConfig, BlockScrollStrategy, ScrollStrategyOptions, ScrollStrategy, CdkOverlayOrigin, FlexibleConnectedPositionStrategyOrigin } from '@angular/cdk/overlay';
import { ComponentPortal, CdkPortal, TemplatePortal } from '@angular/cdk/portal';
// import { ResponsivePositionStrategy } from '../responsive-position-strategy';
import { DynamicOverlay } from '../lib/loader/dynamic-overlay.service';
import { DynamicOverlayContainer } from '../lib/loader/dynamic-overlay-container.service';

import { Service } from 'src/app/service.service'
import { filter } from 'rxjs/operators';
import { ViewportScroller } from '@angular/common';

class Review {
  thumbImage: string;
  name: string;
  review: string;
}

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit, AfterViewChecked, AfterViewInit {

  public env: any = environment;

 // bgImgSource = 'url("' + (this.env.appRoot.length > 0 ? '/' + this.env.appRoot : '') + '/assets/M_Logo.png")';

  bgImg2Source = 'url("' + (this.env.appRoot.length > 0 ? '/' + this.env.appRoot : '') + '/assets/7.jpg")';

  bgImg3Source = 'url("' + (this.env.appRoot.length > 0 ? '/' + this.env.appRoot : '') + '/assets/8.jpg")';

  bgImg4Source = 'url("' + (this.env.appRoot.length > 0 ? '/' + this.env.appRoot : '') + '/assets/1.jpg")';

  bgImg5Source = 'url("' + (this.env.appRoot.length > 0 ? '/' + this.env.appRoot : '') + '/assets/2.jpg")';

  bgImgSources: string[];

  
  public testimonials: Review[] = [
    {
      thumbImage: '/realty/assets/Testimonials/Юлия Корниенко.jpg',
      name: 'Юлия Корниенко',
      review: 'Всякий раз я слушаю материалы практикумов, и это мощно освежает дыхание моего бизнеса. Я уже знаю, если у меня замедлился или остановился какой то бизнес процесс, застрял проект или просто появилось необоснованное желание раньше срока уйти в отпуск - мне нужно на практикум. Тут, профи своего дела, дают столько энергии, что какой там отпуск, включаешь электровоз и вперед. На всех парах активничать. СПАСИБО спикерам - снимаю шляпу перед каждым.. Вас слушать.. Не просто удовольствие, но и немалых размеров вдохновение ДЕЛАТЬ, а не просто слушать, что не всякий тренинг дает. СПАСИБИЩЕ! 😍🥰👍'
    },
    {
      thumbImage: '/realty/assets/Testimonials/Mariya Yamelynets.jpg',
      name: 'Mariya Yamelynets',
      review: 'Друзі,щиро дякую за таку подаровану нам зустріч! Це був неперевершений день,передати словами просто не можливо. Програма дуже повчальна і цікава для кожного реєлтора. Кожен із Вас подарував нам знання і ще більшого прагнення до праці. Емоції просто неможливо передати. Ви для нас взірець тих людей, на яких потрібно рівнятись. До нових зустрічей і успіхів Вам в подальшій праці! Дякуємо!'
    },
    {
      thumbImage: '/realty/assets/Testimonials/Влад Герус.jpg',
      name: 'Влад Герус',
      review: 'Рекомендую всем кто хочет не просто продавать недвижимость, а продавать дорого. Посетил три практикума, и ни разу не возникло мысли что я трачу свое время. Благодарю всех спикеров за то что делятся своим опытом.'
    },
    {
      thumbImage: '/realty/assets/Testimonials/Елена Ботвин.jpg',
      name: 'Елена Ботвин',
      review: 'Была участником обучающего курса" R-Start. Как стать риэлтором". Благодарю спикеров Анну Бондаренко, Александра Недобора, Маргариту Погосбекову, Олега Меньшикова за полученные знания и подход к профессии.'
    },
    {
      thumbImage: '/realty/assets/Testimonials/Елена Волошина.jpg',
      name: 'Елена Волошина',
      review: `Хочу щиро подякувати організаторам за ідею створення практикуму та бажання ділитися знаннями!
      Весь курс сплановано так що після кожної з тем є час на осмислення прослуханого та розуміння своїх подальших кроків в роботі з клієнтами.
      Кожне з занять спікери розбирають на реальних прикладах, так як самі практикуючі рієлтори.
      Я в захваті від курсу!!! Можу і буду рекомендувати колегам! Адже у Вас є чому повчитися!
      Дякую!`
    },
    {
      thumbImage: '/realty/assets/Testimonials/Ирина Наценко.jpg',
      name: 'Ирина Наценко',
      review: `Хочу выразить огромную благодарность всем спикерам курса R-start - Маргарите Погосбековой, Анне Бондаренко, Олегу Меньшикову, Людмиле Федотовой и Александру Недобор.
      Меня давно начала интересовать профессия риэлтора, но сталкиваясь в жизненных ситуациях с агентами, меня всегда пугало их отношение к клиенту и отношения общества к риэлтору. И я, очень рада, что узнала о курсах и прохожу их. То, что делает это команда- это самое ценное, что есть сейчас в наше время - это опыт. Делясь своим опытом , рассказывая поэтапно все, что надо знать о профессии - Они создают профессиональную этику , что является самым ценным . Очень рада, что именно с этого курса и именно эти Спикеры-Риэлторы -Профессионалы начали мой путь к познании этой профессии
      `
    },
    {
      thumbImage: '/realty/assets/Testimonials/Юрий Краковский.jpg',
      name: 'Юрий Краковский',
      review: `Посетил великолепный семинар-практикум для риэлторов! Ведущие - Маргарита Погосбекова и спикеры, Олег
      Меньшиков, Александр Недобор, Елена Маленкова, Александр Гришко, риэлторы-практики, настоящие
      профессионалы, !!! Много было прозрений, озарений, мыслей... Огромная благодарность!!!`,
    },
    {
      thumbImage: '/realty/assets/Testimonials/Татьяна Благая.jpg',
      name: 'Татьяна Благая',
      review: `Участие в Практикуме партнерства стало для меня настоящим открытием!!!!! Живое общение с коллегами, которые умеют решать задачи любой сложности и ГОТОВЫ поделиться тем, как они это делают, дорогого стоит. СПАСИБО за знания и мощный заряд энергии, которым хочется поделиться с другими!!!!!❤️`,
    },
    

  ];

  overlayData = { title: '', loaderType: ''};
  
  scrollStrategy: ScrollStrategy;
  // public emitter: EventEmitter<Observable<boolean>> = new EventEmitter<Observable<boolean>>(false);
  public emitter: EventEmitter<boolean> = new EventEmitter< boolean>(false);
  
  isShowingOverlay: boolean; // Observable<boolean>;
  currentConnectedOrigin: CdkOverlayOrigin;
  
  // constructor () {
  
   @ViewChild('LoaderComponent') loaderComponent: LoaderComponent;

   isPortraitMode: boolean;

   navigateToSubscribe(str: string) {
    let formUrl = ''; 
    if (str === '' || str === undefined) {
       return;
    }
    switch (str) {
      case 'start': formUrl = 'https://forms.gle/GKqpNYnxJ2TPs7i68';
          break;
      case 'workshop': formUrl = 'https://forms.gle/kamEUECZzRUftUeg8';
          break;
      case 'special': formUrl = 'https://forms.gle/YVWyMvUNdAB2rqJC6';
            break;
    }
     window.open(formUrl);
   }
  constructor(public service: Service, public router: Router, private elRef: ElementRef,  private overlay: Overlay, private dynamicOverlay: DynamicOverlay, 
    public readonly sso: ScrollStrategyOptions, private ars: ActivatedRoute, private viewportScroller: ViewportScroller ) {
    
    this.bgImgSources = [ /* this.bgImgSource,*/ this.bgImg2Source, this.bgImg3Source , this.bgImg4Source , this.bgImg5Source ];
    this.scrollStrategy = this.sso.reposition();
    this.emitter.subscribe(sd=>{
      this.isShowingOverlay = sd;
    });
    this.isShowingOverlay = false;
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    if( vw < 1117 ) {
      this.isPortraitMode = true;
    }
    else { 
      this.isPortraitMode = false;
    }
    // this.router.events.pipe(filter(d=> d instanceof NavigationEnd)).subscribe( s=> {
        // if( this.ars.snapshot.fragment && this.ars.snapshot.fragment !== '' ) {
        //   this.router.navigate(['home'], { fragment: this.ars.snapshot.fragment });
        // }
    // })
  }

  ngOnDestroy () {
    this.service.subscription.unsubscribe();
}
  // overlay staff
  @ViewChild(CdkPortal)
  public templatePortal: TemplatePortal;

  @ViewChild(CdkOverlayOrigin)
  public trigger1: CdkOverlayOrigin;
  @ViewChild(CdkOverlayOrigin)
  public trigger2: CdkOverlayOrigin;
  @ViewChild(CdkOverlayOrigin)
  public trigger3: CdkOverlayOrigin;

  closePanelObservable: Observable<boolean> = new Observable<boolean>();
  
  private overlayRef: OverlayRef;
  private ref: OverlayRef;

  
  @ViewChild('frame', { static: false, read: ElementRef }) frame: ElementRef;


  public ngAfterViewInit(): void {
    // this.ars.fragment.subscribe( sbs => {
      if (this.ars.snapshot.fragment && this.ars.snapshot.fragment !== '' ) {
        //this.router.navigate(['home'], { fragment: this.ars.snapshot.fragment });

        

        this.viewportScroller.scrollToAnchor( this.ars.snapshot.fragment );
      }
    // }); 

  }
  public ngAfterViewChecked(): void {
    // this.createOverlay();
    if (this.loaderComponent !== undefined) {
      this.service.subscription.subscribe((x) => {
        this.closeOverlay();
      });
    }
    // this.ars.fragment.subscribe( sbs => {
      // if (this.ars.snapshot.fragment && this.ars.snapshot.fragment !== '' ) {
        // // this.router.navigate(['home'], { fragment: this.ars.snapshot.fragment });
        // this.viewportScroller.scrollToAnchor( this.ars.snapshot.fragment );
      // }
    // }); 
  }

  @HostListener('window:resize')
  public onResize(): void {
  //  this.overlayRef.updatePosition();
  }

  // private createOverlay() {
  //   const config = new OverlayConfig({
  //     positionStrategy: new ResponsivePositionStrategy((config, style, parentStyle) => {

  //       style.position = 'static';
  //       style.marginBottom = '0px';
  //       style.marginRight = `${window.innerWidth/4}px`;

  //       parentStyle.alignItems = 'flex-end';
  //       parentStyle.justifyContent = 'flex-end';

  //     })
  //   });
  //   this.overlayRef = this.overlay.create(config);
  //   this.overlayRef.attach(this.templatePortal);
  // }

  closeOverlay() {
    
    this.emitter.emit( false );
    this.overlayRef.detach();
  }
  showOverlay(type: string) {


    let strName : string, title: string;
    switch( type) {
      case  'Praktikum':
        title = 'Практикум партнерства';
        this.currentConnectedOrigin = this.trigger2;
        break;
      case  'Start':
        title = 'Обучающий курс R-Start';
        this.currentConnectedOrigin = this.trigger1;
        break;
      case  'Special':
        title = 'Специальные программы по различным сферам';
        this.currentConnectedOrigin = this.trigger3;
        break;
      default: 
        break;
    }
    if(!title) {
      return;
    }

    this.overlayData = { title: title, loaderType: type};
    this.isShowingOverlay = true;

    this.refresh(); //this.currentConnectedOrigin
    // this.showSelfOverlay();
   this.emitter.emit(this.isShowingOverlay);
  
    
  }
  
  
   refresh() {
     if(this.overlayRef == undefined) {
       this.overlayRef  = this.dynamicOverlay.createWithDefaultConfig(this.frame.nativeElement);
     }
    //overlayRef = this.dynamicOverlay.createWithDefaultConfig(this.frame.nativeElement);

    if(this.isShowingOverlay) {
      this.overlayRef.attach( this.templatePortal );
    }
  }

  

  closePanel(evt: boolean) {
    // const ref = this.overlayRef.attach<LoaderComponent>(this.templatePortal);
    // this.overlayRef.detach(); // .closePanel.subscribe(() => this.overlayRef.detach());
    //this.closePanelObservable.subscribe(e=> {
      this.closeOverlay();
   // });
    // this.isShowingOverlay = new BehaviorSubject(false); //this.emitter.pipe();
  //  this.emitter.emit(  this.isShowingOverlay );
    // this.emitter.emit(false);
  }
  showGlobalOverlay(origin: any /*FlexibleConnectedPositionStrategyOrigin*/) {
    this.refresh();
    
    // this.overlayRef = this.overlay.create({
    //   scrollStrategy: this.scrollStrategy,
    //   positionStrategy:  this.overlay.position().global().centerHorizontally().centerVertically(), 
    //   //(origin), // centerHorizontally().centerVertically(),
    //   hasBackdrop: true, backdropClass: 'backdropOverlay', panelClass: 'panel-Container'
    // });
    // this.overlayRef.attach( this.templatePortal /*new ComponentPortal(LoaderComponent) */ );
  }

  showSelfOverlay() {
   this.dynamicOverlay.setContainerElement(this.elRef.nativeElement);
  //  this.dynamicOverlay.setContainerElement( dynamicOverlay this.elRef.nativeElement);
   
  //  this.overlayRef = this.dynamicOverlay.create({
  //     positionStrategy: this.dynamicOverlay.position().global().centerHorizontally().centerVertically(),
  //     hasBackdrop: true
  //   });
  //   this.overlayRef.attach( this.templatePortal /* new ComponentPortal(LoaderComponent) */ );
  // }
  }

    public home_page_content: any = {
        advantages_of_our_approach: [],
        title: 'intro-description',
        pictures_in_front: '',
        welcome: ''
    };

    /* 
    "fields": {
    "seo": {
      "title": "Маргарита",
      "meta_description": "Профессиональные услуги, консалтинг и тренер по курсу \"Агенства недвижимости\""
    },
    "": "<ul>\n<li>Номер 1</li>\n<li>Номер два</li>\n<li><iframe width=\"560\" height=\"314\" src=\"//www.youtube.com/embed/wtUr3-cv68g\" allowfullscreen=\"allowfullscreen\"></iframe></li>\n</ul>",
    "pictures_in_front": "https://cdn.buttercms.com/l1hzCk8PSxSTCLLIsmmV",
    "advantages_of_our_approach": [
      {
        "text_for_services": "<h1 class=\"j-blog-header j-blog-headline j-blog-post--headline\">Недвижимость без комиссии</h1>\n<ul>\n<li class=\"j-blog-header j-blog-headline j-blog-post--headline\">описание</li>\n<li class=\"j-blog-header j-blog-headline j-blog-post--headline\">Статьи по теме:</li>\n<li class=\"j-blog-header j-blog-headline j-blog-post--headline\">12 шагов успешной продажи недвижимости. Как происходит продажа квартиры на вторичном рынке?</li>\n</ul>",
        "service_picture": "https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s107259a18929bec5/image/ia60c28b1ecd08973/version/1583923051/image.jpg"
      },
      {
        "text_for_services": "<ul>\n<li>\n<h2><strong>2. Почему я работаю только по договору</strong><strong></strong></h2>\n</li>\n<li><strong>Описание</strong></li>\n</ul>",
        "service_picture": "https://image.jimcdn.com/app/cms/image/transf/dimension=434x1024:format=jpg/path/s107259a18929bec5/image/i437fe9cfcee392a0/version/1537654174/image.jpg"
      },
      {
        "text_for_services": "<h1 class=\"\" id=\"cc-m-header-9164896098\"><img src=\"https://cdn.buttercms.com/w1HskmusTaeVsU9B8XAw\" alt=\"undefined\" class=\"butter-float-right\" /></h1>\n<h2 class=\"j-blog-headline\">1. Услуга для продавца и покупающего недвижимость.</h2>\n<p>В чем наши преимущества:) ?</p>\n<p>Вот пару заголовков из статей, в которых есть много интересного (про продажу и покупку квартир и домов по нашей оптимальной и прогрессивной системе продаж, признаваемой международно.)</p>\n<h2 class=\"j-blog-headline\">В чьих интересах работают агенты, которые рекламируют Вашу квартиру?</h2>\n<p><a href=\"https://www.myrealtykiev.com/2020/07/01/%D0%B2-%D1%87%D1%8C%D0%B8%D1%85-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%B0%D1%85-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D1%8E%D1%82-%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D1%8B-%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5-%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D0%B8%D1%80%D1%83%D1%8E%D1%82-%D0%B2%D0%B0%D1%88%D1%83-%D0%BA%D0%B2%D0%B0%D1%80%D1%82%D0%B8%D1%80%D1%83/\" class=\"j-blog-header\"></a><span></span><time class=\"datetime\" datetime=\"2020-07-01\"><span class=\"day \">01</span><span>&nbsp;</span><span class=\"mon \">Jul</span><span>&nbsp;</span><span class=\"yr \">202</span></time></p>\n<h1 class=\"\" id=\"cc-m-header-9164896098\">Все предлагаемые к продаже объекты - без комиссии с покупателя!</h1>",
        "service_picture": "https://cdn.buttercms.com/mUVZ4WfQlyj9xIMPheho"
      }
    ]
  }
  */
    ngOnInit() {
        // butterService.page.retrieve('page_home', 'home')
            
        // .then((res: any) => {
            const res: any =  { 'fields': {
                      'seo': {
                        'title': 'Маргарита',
                        'meta_description': 'Профессиональные услуги, консалтинг и тренер по курсу "Агенства недвижимости"'
                      },
                      'welcome': '<ul>\n<li>Номер 1</li>\n<li>Номер два</li>\n<li><iframe width="560" height="314" src="//www.youtube.com/embed/wtUr3-cv68g" allowfullscreen="allowfullscreen"></iframe></li>\n</ul>',
                      'pictures_in_front': 'https://cdn.buttercms.com/l1hzCk8PSxSTCLLIsmmV',
                      'advantages_of_our_approach': [
                        {
                          'text_for_services': '<h1 class="j-blog-header j-blog-headline j-blog-post--headline">Недвижимость без комиссии</h1>\n<ul>\n<li class="j-blog-header j-blog-headline j-blog-post--headline">описание</li>\n<li class="j-blog-header j-blog-headline j-blog-post--headline">Статьи по теме:</li>\n<li class="j-blog-header j-blog-headline j-blog-post--headline">12 шагов успешной продажи недвижимости. Как происходит продажа квартиры на вторичном рынке?</li>\n</ul>',
                          'service_picture': 'https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s107259a18929bec5/image/ia60c28b1ecd08973/version/1583923051/image.jpg'
                        },
                        {
                          'text_for_services': '<ul>\n<li>\n<h2><strong>2. Почему я работаю только по договору</strong><strong></strong></h2>\n</li>\n<li><strong>Описание</strong></li>\n</ul>',
                          'service_picture': 'https://image.jimcdn.com/app/cms/image/transf/dimension=434x1024:format=jpg/path/s107259a18929bec5/image/i437fe9cfcee392a0/version/1537654174/image.jpg'
                        },
                        {
                          'text_for_services': '<h1 class="" id="cc-m-header-9164896098"><img src="https://cdn.buttercms.com/w1HskmusTaeVsU9B8XAw" alt="undefined" class="butter-float-right" /></h1>\n<h2 class="j-blog-headline">1. Услуга для продавца и покупающего недвижимость.</h2>\n<p>В чем наши преимущества:) ?</p>\n<p>Вот пару заголовков из статей, в которых есть много интересного (про продажу и покупку квартир и домов по нашей оптимальной и прогрессивной системе продаж, признаваемой международно.)</p>\n<h2 class="j-blog-headline">В чьих интересах работают агенты, которые рекламируют Вашу квартиру?</h2>\n<p><a href="https://www.myrealtykiev.com/2020/07/01/%D0%B2-%D1%87%D1%8C%D0%B8%D1%85-%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%B0%D1%85-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0%D1%8E%D1%82-%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D1%8B-%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5-%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D0%B8%D1%80%D1%83%D1%8E%D1%82-%D0%B2%D0%B0%D1%88%D1%83-%D0%BA%D0%B2%D0%B0%D1%80%D1%82%D0%B8%D1%80%D1%83/" class="j-blog-header"></a><span></span><time class="datetime" datetime="2020-07-01"><span class="day ">01</span><span>&nbsp;</span><span class="mon ">Jul</span><span>&nbsp;</span><span class="yr ">202</span></time></p>\n<h1 class="" id="cc-m-header-9164896098">Все предлагаемые к продаже объекты - без комиссии с покупателя!</h1>',
                          'service_picture': 'https://cdn.buttercms.com/mUVZ4WfQlyj9xIMPheho'
                        }
                      ]
                    }
              };
              const result = res;

              if (result.fields !== undefined) {
                this.home_page_content.title = result.fields.seo.title; //.faq_headline;
                this.home_page_content.welcome = result.fields.welcome; //.faq_headline;

                this.home_page_content.pictures_in_front = result.fields['pictures_in_front'];
                this.home_page_content.advantages_of_our_approach = result.fields['advantages_of_our_approach'];
              } // ).catch(f=>console.log(f));
            //   if(result.data && result.data.fields !== undefined)
            //     this.home_page_content.title = result.data.fields.seo.title; //.faq_headline;
            //     this.home_page_content.welcome = result.data.fields.welcome; //.faq_headline;
                
            //     this.home_page_content.pictures_in_front = result.data.fields['pictures_in_front'];
            //     this.home_page_content.advantages_of_our_approach = result.data.fields['advantages_of_our_approach'];
            // }).catch(f=>console.log(f));

    }
}
