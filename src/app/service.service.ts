import { Injectable, EventEmitter } from '@angular/core';
// import { EventEmitter } from 'events';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public subscription: EventEmitter<boolean>;

  constructor() {
    this.subscription = new EventEmitter<boolean>();
   }

   animateBlock(accordNumber: HTMLDivElement) {
   
    if(accordNumber.classList.contains('animated_Isexpanded')) {
      accordNumber.classList.remove('animated_Isexpanded');
    } else {
      accordNumber.classList.add('animated_Isexpanded');
    }
  }

  blockExpand(blockEvent: any) {
    (blockEvent.target || blockEvent.srcElement || blockEvent.currentTarget ).style.height='auto!important';
  }

}
